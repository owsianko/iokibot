use std::error::Error;
use std::fmt::Error as FmtError;
use std::fmt::{Display, Formatter};

#[macro_export]
macro_rules! err {
    ($x:expr) => {
        if let Err(e) = $x {
            eprintln!("Error occurred: {}", e)
        }
    };
}
#[derive(Debug)]
pub struct StringError(pub String);
impl Display for StringError {
    fn fmt(&self, formatter: &mut Formatter) -> Result<(), FmtError> {
        let StringError(ref string) = self;
        write!(formatter, "{}", string)
    }
}

impl Error for StringError {}

impl StringError {
    pub fn new(s: &str) -> StringError {
        StringError(s.to_string())
    }

    pub fn boxed(s: &str) -> Box<StringError> {
        Box::new(StringError::new(s))
    }
}

#[derive(Debug)]
pub struct ConfigError;
impl Display for ConfigError {
    fn fmt(&self, formatter: &mut Formatter) -> Result<(), FmtError> {
        write!(formatter, "ConfigError")
    }
}

impl Error for ConfigError {}
