use crate::badge::Tier;
use crate::bot_config::BotConfig;

pub enum Command {
    StartEntries,
    StopEntries,
    AcceptNewEntries,
    PrintConfig,
    ChangeCommand(String),
    ChangeRigSubs(bool),
    ChangeMinimumTier(Tier),
    ChangeVerbose(bool),
    // Only used it the web client
    SetConfig(BotConfig),
}
