use std::collections::HashMap;
use std::collections::HashSet;

use rand::Rng;

#[derive(Debug)]
pub struct ContestantMap {
    underlying: HashMap<String, u32>,
}

impl ContestantMap {
    pub fn new() -> ContestantMap {
        let underlying = HashMap::new();
        ContestantMap { underlying }
    }

    fn sum(&self) -> u64 {
        let mut res = 0;
        for val in self.underlying.values() {
            res += *val as u64;
        }
        res
    }

    pub fn pick_and_remove<R: Rng>(&mut self, rng: &mut R) -> String {
        let winner = self.get_random(rng).to_owned();
        self.remove(&winner);
        winner
    }

    pub fn get_random<R: Rng>(&self, rng: &mut R) -> &str {
        let sum = self.sum();
        if sum > 0 {
            let mut goal = rng.gen_range(0, sum);
            for (k, v) in self.underlying.iter() {
                let val: u64 = *v as u64;
                if goal <= val {
                    return k;
                } else {
                    goal -= val;
                }
            }
            ""
        } else {
            ""
        }
    }

    /*
    pub fn insert(&mut self, contestant: String) {
        self.insert_multiple(contestant, 1)
    }
    */

    pub fn insert(&mut self, mut contestant: String, entries: u32) {
        if entries > 0 {
            contestant.make_ascii_lowercase();
            match self.underlying.get_mut(&contestant) {
                Some(c) => *c += entries,
                None => {
                    self.underlying.insert(contestant, entries);
                }
            }
        }
    }

    pub fn remove(&mut self, contestant: &str) {
        let actual_contestant = contestant.to_lowercase();
        self.underlying.remove(&actual_contestant);
    }

    pub fn remove_one(&mut self, contestant: &str) {
        let actual_contestant = contestant.to_lowercase();
        if let Some(ct) = self.underlying.get_mut(&actual_contestant) {
            if *ct > 1 {
                *ct -= 1
            } else {
                self.underlying.remove(&actual_contestant);
            }
        }
    }

    pub fn clear(&mut self) {
        self.underlying.clear();
    }

    pub fn get_all(&self) -> HashSet<(&String, &u32)> {
        let mut result = HashSet::new();
        for kv in self.underlying.iter() {
            result.insert(kv);
        }
        result
    }

    pub fn get_all_list(&self) -> Vec<&String> {
        let mut result = Vec::new();
        for (k, v) in self.underlying.iter() {
            for _ in 0..*v {
                result.push(k);
            }
        }
        result
    }
}

#[cfg(test)]
mod contestant_map_suite {
    use super::ContestantMap;

    const NUM_DISTINCT_ENTRIES: usize = 2;
    const NUM_JOHN_ENTRIES: u32 = 2;
    const NUM_ALICE_ENTRIES: u32 = 1;
    const NUM_ENTRIES_TOTAL: usize = 3;
    fn setup() -> ContestantMap {
        let mut cmap = ContestantMap::new();
        cmap.insert("john".to_string(), NUM_JOHN_ENTRIES);
        cmap.insert("alice".to_string(), NUM_ALICE_ENTRIES);
        cmap
    }

    #[test]
    fn test_get_all() {
        let cmap = setup();
        let all = cmap.get_all();
        assert!(all.contains(&(&"john".to_string(), &NUM_JOHN_ENTRIES)));
        assert!(all.contains(&(&"alice".to_string(), &NUM_ALICE_ENTRIES)));
        assert!(!all.contains(&(&"john".to_string(), &1)));
        assert_eq!(all.len(), NUM_DISTINCT_ENTRIES);
    }

    #[test]
    fn test_get_all_list() {
        let cmap = setup();
        let v = cmap.get_all_list();
        assert_eq!(v.len(), NUM_ENTRIES_TOTAL);
        let mut alice_entries = 0;
        let mut john_entries = 0;
        for val in v {
            if val == "john" {
                john_entries += 1;
            } else if val == "alice" {
                alice_entries += 1;
            } else {
                assert!(false);
            }
        }
        assert_eq!(alice_entries, NUM_ALICE_ENTRIES);
        assert_eq!(john_entries, NUM_JOHN_ENTRIES);
    }

    #[test]
    fn test_caps() {
        let mut cmap = ContestantMap::new();
        cmap.insert("JOHN".to_string(), 1);
        cmap.insert("jOhn".to_string(), 1);
        cmap.insert("johN".to_string(), 1);
        let all = cmap.get_all();
        assert_eq!(all.len(), 1);
        assert!(all.contains(&(&"john".to_string(), &3)))
    }
}
