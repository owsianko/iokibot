use crate::error::*;
use std::error::Error;
use std::fs::read_to_string;
use std::io::{BufRead, Write};

pub struct TwitchInfo {
    pub username: String,
    pub oauth: String,
    pub channel: String,
}

const CONFIG_FILE: &str = "config.txt";

pub fn get_twitchinfo() -> Result<TwitchInfo, Box<dyn Error>> {
    let (username, oauth, channel) = {
        if let Ok(file) = read_to_string(CONFIG_FILE) {
            let mut lines = file.lines();
            let username = lines.next().ok_or(ConfigError)?;
            let channel = lines.next().ok_or(ConfigError)?;
            let oauth = match lines.next() {
                Some(o) => o.to_string(),
                None => rpassword::read_password_from_tty(Some(
                    "Enter oauth token (obtainable here: https://twitchapps.com/tmi/): ",
                ))?,
            };
            (username.to_string(), oauth, channel.to_string())
        } else {
            println!(
                "Could not open standard configuration file ({})",
                CONFIG_FILE
            );
            create_config()?
        }
    };
    Ok(TwitchInfo {
        username,
        oauth,
        channel,
    })
}

fn create_config() -> Result<(String, String, String), Box<Error>> {
    println!("Creating new configuration file...");
    let stdin = std::io::stdin();
    let mut lines = stdin.lock().lines();
    println!("Please input your username:");
    let username: String = lines
        .next()
        .ok_or(StringError::boxed("Username cannot be empty"))??;
    println!("Which channel should the bot operate in? Default: same as username");
    let raw_channel = lines.next().ok_or(username.clone())??;
    let channel = if raw_channel.len() == 0 {
        username.clone()
    } else {
        raw_channel
    };
    println!("Please enter your oauth token (obtainable here: https://twitchapps.com/tmi/)");
    //println!("Note: your password won't appear as you type it");
    //let password = rpassword::read_password_from_tty(None)?;
    let password = lines.next().ok_or(StringError::boxed("Please input a token"))??;
    println!("Do you want to save your password/oauth along with the rest of the config? [y/N]");
    println!("Note: this will store your password/oauth as plain text.");
    let write_pw = if let Some(Ok(resp)) = lines.next() {
        resp.starts_with("y") || resp.starts_with("Y")
    } else {
        false
    };

    match std::fs::File::create(CONFIG_FILE) {
        Ok(mut f) => {
            err!(writeln!(f, "{}", username));
            err!(writeln!(f, "{}", channel));
            if write_pw {
                err!(writeln!(f, "{}", password));
            };
        }
        Err(_) => eprintln!("Could not open file for writing: {}", CONFIG_FILE),
    };
    Ok((username, password.to_string(), channel))
}
