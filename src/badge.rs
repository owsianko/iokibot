use std::error::Error;

#[derive(Hash, Eq, PartialEq, Debug)]
pub enum Badge {
    Subscriber(Tier),
    Mod,
    Streamer,
}

impl Badge {
    pub fn from_tchat_badge(b: &twitchchat::Badge) -> Option<Badge> {
        match b.kind {
            twitchchat::BadgeKind::Broadcaster => Some(Badge::Streamer),
            twitchchat::BadgeKind::Moderator => Some(Badge::Mod),
            twitchchat::BadgeKind::Subscriber => {
                Tier::from_str(&b.data).ok().map(|t| Badge::Subscriber(t))
            }
            _ => None,
        }
    }
}

#[derive(Eq, PartialEq, Hash, PartialOrd, Clone, Copy, Debug)]
pub enum Tier {
    NoSub,
    One,
    Two,
    Three,
    Unreachable,
}

impl Tier {
    pub fn from_u16(u: u16) -> Result<Tier, Box<dyn Error>> {
        match u {
            0 => Ok(Tier::NoSub),
            1 => Ok(Tier::One),
            2 => Ok(Tier::Two),
            3 => Ok(Tier::Three),
            _ => Ok(Tier::Unreachable),
        }
    }

    pub fn from_str(s: &str) -> Result<Tier, Box<dyn Error>> {
        let u = s.parse()?;
        Tier::from_u16(u)
    }

    pub fn to_u16(&self) -> u16 {
        match self {
            Tier::NoSub => 0,
            Tier::One => 1,
            Tier::Two => 2,
            Tier::Three => 3,
            Tier::Unreachable => u16::max_value(),
        }
    }
}
use std::cmp::Ordering;
impl Ord for Tier {
    fn cmp(&self, other: &Tier) -> Ordering {
        self.to_u16().cmp(&other.to_u16())
    }
}
