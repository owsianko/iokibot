extern crate twitchchat;
use std::error::Error;
use std::io::stdin;
use std::sync::mpsc;
use std::sync::{Arc, Mutex};
use std::thread;

extern crate rand;
use rand::thread_rng;

mod badge;
mod bot_config;
mod command;
mod contestant_map;
#[macro_use]
mod error;
mod http_server;
mod iokibot;
mod twitchinfo;

use command::Command;
use contestant_map::*;
use error::StringError;
pub fn main_loop() -> Result<(), Box<dyn Error>> {
    let excluded_set = std::collections::HashSet::new();
    let contestant_map = Arc::new(Mutex::new((ContestantMap::new(), excluded_set)));

    let cloned_cmap = Arc::clone(&contestant_map);
    let (sender, receiver) = mpsc::channel();
    let (kill_sender, kill_receiver) = mpsc::channel();

    let twitchinfo = twitchinfo::get_twitchinfo()?;

    // start bot thread
    let first_kill_sender = kill_sender.clone();
    thread::spawn(move || {
        err!(iokibot::run_bot(cloned_cmap, receiver, twitchinfo));
        println!("Press Enter to continue...");
        first_kill_sender.send(()).unwrap();
    });

    let cloned_sender = sender.clone();
    let cloned_cmap = Arc::clone(&contestant_map);
    thread::spawn(move || {
        err!(http_server::run(cloned_cmap, cloned_sender));
        kill_sender.send(()).unwrap();
    });

    let stdin = stdin();
    let mut rng = thread_rng();
    loop {
        let mut buffer = String::new();
        // stop the bot
        if let Ok(_) = kill_receiver.try_recv() {
            println!("Shutting bot down...");
            break;
        }

        if let Ok(_) = stdin.read_line(&mut buffer) {
            let buffer = buffer.trim();
            let words = buffer.split(' ').collect::<Vec<_>>();
            if words.len() > 0 {
                match words[0] {
                    "exit" => break,
                    "list" => {
                        let cmap = &contestant_map.lock().unwrap().0;
                        print_contestants(cmap);
                    }
                    "list_excluded" => {
                        let _excluded_set = &contestant_map.lock().unwrap().1;
                        // TODO
                    }
                    "set" => match process_set_command(&words) {
                        Ok(comm) => sender.send(comm).unwrap(),
                        Err(s) => println!("{}", s),
                    },
                    "start" => sender.send(Command::StartEntries).unwrap(),
                    "draw" => {
                        let cmap = &mut contestant_map.lock().unwrap().0;
                        let winner = cmap.pick_and_remove(&mut rng);
                        println!("{}", winner);
                    }
                    "print-settings" => sender.send(Command::PrintConfig).unwrap(),
                    "accept-new" => sender.send(Command::AcceptNewEntries).unwrap(),
                    "stop" => sender.send(Command::StopEntries).unwrap(),
                    _ => {
                        if buffer.len() > 0 {
                            println!("Command not found, {:?}", buffer)
                        }
                    }
                }
            } else {
                println!("Please input a command");
            }
        }
    }
    Ok(())
}

fn process_set_command(words: &Vec<&str>) -> Result<Command, Box<Error>> {
    debug_assert!(words[0] == "set");
    if words.len() < 3 {
        Err(StringError::boxed("Invalid usage! Set expects 2 arguments"))
    } else {
        match words[1] {
            // todo fix
            "command" => Ok(Command::ChangeCommand(words[2].to_owned())),
            "rig-subs" => Ok(Command::ChangeRigSubs(str_to_bool(words[2])?)),
            "verbose" => Ok(Command::ChangeVerbose(str_to_bool(words[2])?)),
            "minimum-tier" => Ok(Command::ChangeMinimumTier(crate::badge::Tier::from_str(
                words[2],
            )?)),
            _ => Err(StringError::boxed("Invalid parameter")),
        }
    }
}

fn str_to_bool(scrut: &str) -> Result<bool, StringError> {
    match scrut {
        "true" | "t" => Ok(true),
        "false" | "f" => Ok(false),
        _ => Err(StringError::new(
            "Could not parse boolean. Possible values true/t or false/f",
        )),
    }
}

fn print_contestants(contestant_map: &ContestantMap) {
    let all = contestant_map.get_all();
    if all.len() == 0 {
        println!("(No entries)");
    }
    for (c, e) in all {
        println!("{}: {}", c, e)
    }
}
