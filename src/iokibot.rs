use super::badge::Badge;
use super::bot_config::BotConfig;
use super::command::Command;
use super::contestant_map::ContestantMap;
use crate::twitchinfo::TwitchInfo;
use std::collections::HashSet;
use std::sync::mpsc::Receiver;
use std::thread;

use std::error::Error;
use std::net::TcpStream;
use std::sync::{Arc, Mutex};
use twitchchat::commands::PrivMsg;
use twitchchat::{sync_adapters, Client, UserConfig, Writer};

extern crate rpassword;
struct BotParams {
    already_entered_users: HashSet<String>,
    allow_entries: bool,
}

pub fn run_bot(
    contestants: Arc<Mutex<(ContestantMap, HashSet<String>)>>,
    commands: Receiver<Command>,
    twitchinfo: TwitchInfo,
) -> Result<(), Box<dyn Error>> {
    let TwitchInfo {
        username,
        oauth,
        channel,
    } = twitchinfo;
    let userconfig = UserConfig::builder()
        .nick(username)
        .token(oauth)
        .tags()
        .build()
        .unwrap(); // fight me

    let bot_config = Arc::new(Mutex::new(BotConfig::load_default()));

    let mut client = {
        let tcp_read = TcpStream::connect(twitchchat::TWITCH_IRC_ADDRESS)?;
        let tcp_write = tcp_read.try_clone()?;
        let (read, write) = sync_adapters(tcp_read, tcp_write);
        Client::new(read, write)
    };

    client.register(userconfig)?;
    let default_params = BotParams {
        allow_entries: false,
        already_entered_users: HashSet::new(),
    };
    let writer = client.writer();
    writer.join(channel.clone())?;

    // create new paramaters - those are more like runtime variables
    let bot_params = Arc::new(Mutex::new(default_params));

    // prepare to move them into the thread
    let bot_params_clone = Arc::clone(&bot_params);
    let bot_config_clone = Arc::clone(&bot_config);
    let contestants_clone = Arc::clone(&contestants);
    thread::spawn(move || {
        let botparams = bot_params_clone;
        let botconfig = bot_config_clone;
        use Command::*;
        loop {
            if let Ok(comm) = commands.recv() {
                let mut params = botparams.lock().unwrap();
                let mut config = botconfig.lock().unwrap();
                match comm {
                    StartEntries => {
                        params.allow_entries = true;
                        let mut contestants = contestants_clone.lock().unwrap();
                        contestants.1.clear();
                        contestants.0.clear();

                        if config.verbose {
                            err!(writer.privmsg(
                                channel.clone(),
                                format!(
                                    "Now accepting entries! Type \"{}\" in order to enter!",
                                    config.current_command
                                )
                            ))
                        }
                    }
                    PrintConfig => eprintln!("{:#?}", *config),
                    StopEntries => {
                        params.allow_entries = false;
                        if config.verbose {
                            err!(writer.privmsg(channel.clone(), "No longer accepting entries!"))
                        }
                    }
                    AcceptNewEntries => {
                        // just in case it wasn't already on
                        params.allow_entries = true;
                        params.already_entered_users = HashSet::new();
                        if config.verbose {
                            err!(writer.privmsg(channel.clone(), "Now accepting NEW entries\
                                . If you've already entered, you can now enter again for another entry!"))
                        }
                    }
                    ChangeMinimumTier(t) => config.minimum_tier = t,
                    ChangeCommand(new_comm) => config.current_command = new_comm,
                    ChangeRigSubs(b) => config.rig_subs = b,
                    ChangeVerbose(b) => config.verbose = b,
                    SetConfig(c) => *config = c,
                }
            }
        }
    });

    client.on(move |msg: PrivMsg, _w: Writer| {
        // no thread can fail
        let cfg = bot_config.lock().unwrap();
        let mut params = bot_params.lock().unwrap();
        if msg.message().starts_with(&cfg.current_command) {
            if params.allow_entries {
                if !params.already_entered_users.contains(msg.user()) {
                    let mut tuple = contestants.lock().unwrap();
                    if !tuple.1.contains(msg.user()) {
                        // get useful badges
                        let badges: Vec<Badge> = msg
                            .badges()
                            .iter()
                            .map(|b| Badge::from_tchat_badge(b))
                            .filter(|o| o.is_some())
                            .map(|o| o.unwrap())
                            .collect();

                        let riggage = cfg.get_riggage(&badges);
                        tuple.0.insert(msg.user().to_string(), riggage);
                        params.already_entered_users.insert(msg.user().to_string());
                    }
                }
            }
        }
    });

    if let Err(e) = client.run() {
        Err(Box::new(e))
    } else {
        Ok(())
    }
}
