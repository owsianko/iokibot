use super::badge::*;
use std::collections::HashSet;
use std::io::Write;

#[derive(Debug, Clone)]
pub struct BotConfig {
    pub current_command: String,
    pub rig_subs: bool,
    pub minimum_tier: Tier,
    pub allow_mods: bool,
    pub verbose: bool,
}

impl BotConfig {
    const DEFAULT_SETTINGS_PATH: &'static str = "default_settings.txt";

    pub fn load_default() -> BotConfig {
        if let Ok(file) = std::fs::read_to_string(BotConfig::DEFAULT_SETTINGS_PATH) {
            let mut lines = file.lines();
            let current_command = lines.next().unwrap_or("!entry").to_string();
            let rig_subs = lines.next().map(|e| e.starts_with("true")).unwrap_or(false);
            let minimum_tier = lines
                .next()
                .and_then(|e| Tier::from_str(e).ok())
                .unwrap_or(Tier::One);
            let allow_mods = lines.next().map(|e| e.starts_with("true")).unwrap_or(false);
            let verbose = lines.next().map(|e| e.starts_with("true")).unwrap_or(false);
            BotConfig {
                current_command,
                rig_subs,
                minimum_tier,
                allow_mods,
                verbose,
            }
        } else {
            BotConfig::default()
        }
    }

    pub fn to_json(&self) -> String {
        format!(
            r#"{{
            "command": "{0}",
            "rig_subs": {1},
            "minimum_tier": {2},
            "allow_mods": {3},
            "verbose": {4}
        }}"#,
            self.current_command,
            self.rig_subs,
            self.minimum_tier.to_u16(),
            self.allow_mods,
            self.verbose
        )
    }

    pub fn save_as_default(&self) {
        if let Ok(mut file) = std::fs::File::create(Self::DEFAULT_SETTINGS_PATH) {
            let _ = writeln!(file, "{}", self.current_command);
            let _ = writeln!(file, "{}", self.rig_subs);
            let _ = writeln!(file, "{}", self.minimum_tier.to_u16());
            let _ = writeln!(file, "{}", self.allow_mods);
            let _ = writeln!(file, "{}", self.verbose);
        }
    }

    pub fn default() -> BotConfig {
        let mut allowed_badges = HashSet::new();
        allowed_badges.insert(Badge::Subscriber(Tier::One));
        BotConfig {
            current_command: "!entry".to_string(),
            rig_subs: false,
            minimum_tier: Tier::One,
            allow_mods: false,
            verbose: true,
        }
    }

    pub fn empty() -> BotConfig {
        BotConfig {
            current_command: "".to_string(),
            rig_subs: false,
            minimum_tier: Tier::Unreachable,
            allow_mods: false,
            verbose: false,
        }
    }

    pub fn get_riggage(&self, badges: &Vec<Badge>) -> u32 {
        let is_mod = badges.iter().filter(|b| **b == Badge::Mod).count() > 0;
        let odds = badges
            .iter()
            .filter_map(|b| {
                if let Badge::Subscriber(t) = b {
                    Some(t)
                } else {
                    None
                }
            })
            .filter(|t| **t >= self.minimum_tier)
            .map(|t| t.to_u16() - self.minimum_tier.to_u16())
            .map(|t| t as u32)
            .map(|t| if t == 0 { 1 } else { t * 2 })
            .fold(0, |acc, el| el + acc);

        if odds == 0 {
            if is_mod && self.allow_mods {
                1
            } else {
                if self.minimum_tier > Tier::NoSub {
                    0
                } else {
                    1
                }
            }
        } else {
            if self.rig_subs {
                odds.into()
            } else {
                1
            }
        }
    }
}

#[cfg(test)]
mod rig_test {
    use super::*;
    #[test]
    fn test_viewer_no_rig() {
        let config = BotConfig {
            current_command: "".to_string(),
            rig_subs: false,
            minimum_tier: Tier::NoSub,
            allow_mods: false,
            verbose: true,
        };
        let badges_case1 = vec![Badge::Subscriber(Tier::Three), Badge::Streamer];
        let badges_case2 = vec![Badge::Subscriber(Tier::One)];
        let badges_case3 = vec![Badge::Mod];
        assert_eq!(config.get_riggage(&badges_case1), 1);
        assert_eq!(config.get_riggage(&badges_case2), 1);
        assert_eq!(config.get_riggage(&badges_case3), 1);
    }
}

#[cfg(test)]
mod tier_test {
    use super::Tier::{self, *};

    fn check_anti_symmetry(a: Tier, b: Tier) {
        let gt = a > b;
        let lt = a < b;
        let eq = a == b;
        assert!(gt || lt || eq);
        assert!(!gt || !lt);
        assert!(!gt || !eq);
        assert!(!lt || !eq);
    }

    fn check_bidir_anty_symmetry(a: Tier, b: Tier) {
        check_anti_symmetry(a, b);
        check_anti_symmetry(b, a);
    }

    #[test]
    fn test_anti_symmetry() {
        check_bidir_anty_symmetry(One, Two);
        check_bidir_anty_symmetry(One, Three);
        check_bidir_anty_symmetry(Two, Three);
    }

    #[test]
    fn check_transitivity() {
        assert!(One < Two);
        assert!(One < Three);
        assert!(One == One);
        assert!(Two < Three);
        assert!(Two == Two);
        assert!(Three == Three);
    }
}
