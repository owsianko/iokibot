extern crate multipart;
extern crate tiny_http;
use multipart::server::Multipart;
use rand::Rng;
use std::error::Error;
use std::fs::read_to_string;
use std::io::Read;
use std::str::FromStr;
use std::sync::mpsc::Sender;
use tiny_http::*;

use crate::badge::Tier;
use crate::bot_config::BotConfig;
use crate::command::Command;
use crate::contestant_map::ContestantMap;
use crate::error::StringError;

type Resp = Response<std::io::Cursor<Vec<u8>>>;
type ContestantMutex =
    std::sync::Arc<std::sync::Mutex<(ContestantMap, std::collections::HashSet<String>)>>;
pub fn run(
    contestants_mutex: ContestantMutex,
    command_sender: Sender<Command>,
) -> Result<(), Box<Error>> {
    let mut try_port = 6969;
    let max_tries = 100;
    let mut try_count = 0;
    let mut server;

    loop {
        match Server::http(format!("localhost:{}", try_port)) {
            Ok(s) => {
                server = s;
                break;
            }
            Err(e) => {
                if max_tries > try_count {
                    try_count += 1;
                    try_port += 1;
                } else {
                    return Err(Box::new(StringError(format!(
                        "Could not launch server; error: {}",
                        e
                    ))));
                }
            }
        };
    }

    let index = read_to_string("index.html")?;
    let jquery = read_to_string("jquery.js")?;
    let css = read_to_string("style.css")?;

    println!("Server running on http://localhost:{}", try_port);

    // let html_header = Header::from_str("Content-Type: text/html").unwrap();
    let css_header = Header::from_str("Content-Type: text/css").unwrap();
    let html_header: Header = Header::from_str("Content-Type: text/html").unwrap();
    let js_header = Header::from_str("Content-Type: Application/javascript").unwrap();
    let mut rng = rand::thread_rng();

    loop {
        match server.recv() {
            Ok(mut rq) => match rq.method() {
                Method::Get => {
                    let resp = match rq.url() {
                        "/jquery.js" => Response::from_string(jquery.clone())
                            .with_status_code(200)
                            .with_header(js_header.clone()),

                        "/list" => get_list_response(&contestants_mutex),
                        "/list_json" => get_json_of_contestants(&contestants_mutex),
                        "/list_excluded" => get_list_excluded(&contestants_mutex),

                        "/style.css" => Response::from_string(css.clone())
                            .with_status_code(200)
                            .with_header(css_header.clone()),

                        "/default_settings" => default_settings(&BotConfig::load_default()),

                        "/" => Response::from_string(index.clone())
                            .with_status_code(200)
                            .with_header(html_header.clone()),
                        _ => Response::from_string("").with_status_code(404),
                    };
                    let _ = rq.respond(resp);
                }
                Method::Head => {
                    let resp = Response::empty(200);
                    let _ = rq.respond(resp);
                }
                Method::Post => {
                    let resp = get_response(&mut rq, &command_sender, &contestants_mutex, &mut rng);
                    let _ = rq.respond(resp);
                }
                _ => (),
            },
            Err(e) => eprintln!("Error has occurred: {}", e),
        }
    }
}

fn get_list_excluded(contestants: &ContestantMutex) -> Resp {
    let excluded = &contestants.lock().unwrap().1;
    let mut list: Vec<_> = excluded.iter().collect();
    list.sort();
    let mut result_body = String::new();
    for c in list {
        let c_html = format!("<p>{}</p>", c);
        result_body.push_str(&c_html);
    }
    Response::from_string(result_body)
        .with_status_code(200)
        .with_header(Header::from_str("Content-Type: text/html").unwrap())
}

/// gets the list of centered people
fn get_list_response(contestants: &ContestantMutex) -> Resp {
    let contestant_map = &contestants.lock().unwrap().0;
    let set = contestant_map.get_all();
    let mut list = set.iter().collect::<Vec<_>>();
    list.sort_by(|a, b| a.0.cmp(b.0));
    let mut result_body = String::new();
    for contestant in list {
        let contestant_html = format!("<p>{} x{}</p>", contestant.0, contestant.1);
        result_body.push_str(&contestant_html);
    }
    Response::from_string(result_body)
        .with_status_code(200)
        .with_header(Header::from_str("Content-Type: text/html").unwrap())
}

/// get a json-formatted array of participants
fn get_json_of_contestants(contestants_mutex: &ContestantMutex) -> Resp {
    let cmap = &contestants_mutex.lock().unwrap().0;
    let contestant_list = cmap.get_all_list();
    let list = contestant_list
        .iter()
        .map(|el| (*el).clone())
        .collect::<Vec<String>>()
        .join("\",\"");

    let result_body = if list.len() > 0 {
        format!("[\"{}\"]", list)
    } else {
        "[]".to_string()
    };

    Response::from_string(result_body)
        .with_status_code(200)
        .with_header(Header::from_str("Content-Type: application/json").unwrap())
}

/// Generic, empty OK response
fn ok_resp() -> Resp {
    let text_header: Header = Header::from_str("Content-Type: text/plain").unwrap();
    Response::from_string("")
        .with_status_code(200)
        .with_header(text_header)
}

fn error_resp() -> Resp {
    ok_resp().with_status_code(400)
}

fn default_settings(settings: &BotConfig) -> Resp {
    let json_header: Header = Header::from_str("Content-Type: application/json").unwrap();
    Response::from_string(settings.to_json())
        .with_status_code(200)
        .with_header(json_header)
}

fn get_response<R: Rng>(
    rq: &mut Request,
    cmd_sender: &Sender<Command>,
    contestants_mutex: &ContestantMutex,
    rng: &mut R,
) -> Resp {
    let text_header: Header = Header::from_str("Content-Type: text/plain").unwrap();
    let url = rq.url();
    match url {
        "/remove_all" | "/remove_one" | "/remove_exclude" => {
            let is_blacklist = url == "/remove_exclude";
            let is_all = is_blacklist || url == "/remove_all";
            let mut buffer = String::new();
            if let Ok(_) = rq.as_reader().read_to_string(&mut buffer) {
                let mut tuple = contestants_mutex.lock().unwrap();
                {
                    let cmap = &mut tuple.0;
                    if is_all {
                        cmap.remove(&buffer);
                    } else {
                        cmap.remove_one(&buffer);
                    }
                }

                let excluded = &mut tuple.1;
                if is_blacklist {
                    excluded.insert(buffer);
                }

                ok_resp()
            } else {
                error_resp()
            }
        }

        "/pick_remove" => {
            let cmap = &mut contestants_mutex.lock().unwrap().0;
            let winner = cmap.pick_and_remove(rng);
            Response::from_string(winner)
                .with_header(text_header)
                .with_status_code(200)
        }
        "/pick" => {
            let cmap = &contestants_mutex.lock().unwrap().0;
            let winner = cmap.get_random(rng);
            Response::from_string(winner)
                .with_header(text_header)
                .with_status_code(200)
        }
        "/start_entries" => {
            cmd_sender.send(Command::StartEntries).unwrap();
            ok_resp()
        }
        "/stop_entries" => {
            cmd_sender.send(Command::StopEntries).unwrap();
            ok_resp()
        }
        "/accept_new" => {
            cmd_sender.send(Command::AcceptNewEntries).unwrap();
            ok_resp()
        }
        "/set_settings" => {
            if let Ok(mut multipart) = Multipart::from_request(rq) {
                match extract_settings(&mut multipart) {
                    Err(e) => eprintln!("{}", e),
                    Ok(config) => {
                        config.save_as_default();
                        cmd_sender.send(Command::SetConfig(config)).unwrap()
                    }
                }
                ok_resp()
            } else {
                ok_resp().with_status_code(400) // another hack
            }
        }
        _ => ok_resp().with_status_code(404), // yes this is a hack lmao
    }
}

/// extracts settings from a multipart form-data
fn extract_settings<R: Read>(multipart: &mut Multipart<R>) -> Result<BotConfig, Box<dyn Error>> {
    let mut config = BotConfig::empty();
    multipart.foreach_entry(|ref mut field| {
        if field.is_text() {
            let mut value = String::new();
            if let Err(_) = field.data.read_to_string(&mut value) {
                return;
            }
            let key = field.headers.name.to_string();
            match key.as_ref() {
                "command" => config.current_command = value,
                "sub_tier" => {
                    if let Ok(t) = Tier::from_str(&value) {
                        config.minimum_tier = t;
                    }
                }
                "mod_checkbox" => config.allow_mods = true,
                "rig_subs" => config.rig_subs = true,
                "verbose" => config.verbose = true,
                _ => (),
            }
        }
    })?;
    if config.current_command.len() == 0 {
        Err(StringError::boxed("No Command given"))
    } else {
        Ok(config)
    }
}
